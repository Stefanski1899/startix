/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

// BUG: For some reason if we don't add a frotend for connman, it won't work. Ask the community why
// TODO: Look up popen() and fork()
// TODO: Check if we can improve the code in locale

#include "commons.hpp"
#include "extra.hpp"
#include "get-data.hpp"
#include "info.hpp"
#include "locale/locales.hpp"

void installation_completed() {
    cout << green + "\nThe installation process was completed!" + end_color << endl;
    cout << green + "Thanks for using the installer!" + end_color << endl;
    system("rm -r startix");
    system("rm startix-bin");
}

int main() {

    string tmp, keymap, disk, init;
    string devel, kernel, city, username, hostname;

    tmp = get_option(welcome_massage(), sections, true, "START");

    while (true) {
        if (tmp == "START" || tmp == "Partitions")
            goto Partitions;
        else if (tmp == "HELP") {
            help_info();
            tmp = get_option("Select a label: ", sections, true, "START");
        } else if (tmp == "Format")
            goto Format;
        else if (tmp == "Mount")
            goto Mount;
        else if (tmp == "Internet")
            goto Internet;
        else if (tmp == "Base")
            goto Base;
        else if (tmp == "Kernel")
            goto Kernel;
        else if (tmp == "FSTAB")
            goto FSTAB;
        else if (tmp == "CHROOT")
            goto CHROOT;
        else if (tmp == "Config")
            goto Config;
        else if (tmp == "Bootloader")
            goto Bootloader;
        else if (tmp == "User")
            goto User;
        else if (tmp == "Network")
            goto Network;
        else if (tmp == "Extra")
            goto Extra;
    }

    Partitions:
        get_disk(disk);
        tmp = get_option("Do you want to use a program to partition the disk? [fdisk/cfdisk/no] (default: cfdisk) ",
                vector<string>{"cfdisk", "fdisk", "no"}, true, "cfdisk");

        if (tmp != "no") {
            system("umount /mnt"); // This is off while testing
            system((tmp + " /dev/" + disk).c_str());
        }

    Format:
        cout << blue + "\nFormat your partitions. Every line will get passed as it is\n" + end_color;
        cout << blue + "When you're done, type 'DONE' to continue\n" + end_color;
        getline(cin, tmp);

        while (tmp != "DONE") {
            system(tmp.c_str());
            getline(cin, tmp);
        }

    Mount:
        system("mount /dev/disk/by-label/ROOT /mnt");
        cout << green + "\nThe root partition has been mounted successfully!" + end_color;
        tmp = get_option("\nDo you want to mount any more partitions? [y-yes/n-no] ", y_n, false, "");

        if (tmp == "y" || tmp == "yes") {
            cout << blue + "\nMount your partitions. Every line will get passed as it is\n" + end_color;
            cout << blue + "When you're done, type 'DONE' to continue" + end_color << endl;
            getline(cin, tmp);

            while (tmp != "DONE") {
                system(tmp.c_str());
                getline(cin, tmp);
              }
          }

    Internet:
        cout << blue + "\nConnect to the internet (if needed). Every line will get passed as it is" + end_color << endl;
        cout << blue + "When you're done, type 'DONE' to continue " + end_color;
        getline(cin, tmp);

        while (tmp != "DONE") {
            system(tmp.c_str());
            getline(cin, tmp);
        }

    Base:
        cout << blue + "\nInstalling base system! " + end_color << endl;
        get_init(init);
        devel = get_option("Add base-devel? [y-yes/n-no] (default: no) ", y_n, true, "no");

        tmp = "basestrap /mnt ";
        tmp = (devel == "y" || devel == "yes") ? (tmp + "base base-devel " + init) : (tmp + "base " + init);

        if (init == "runit") {
            tmp += " elogind-runit";
        } else if (init == "s6") {
            tmp += " elogind-s6";
        } system(tmp.c_str());

    Kernel:
        kernel = get_option("\nWhich kernel you want to install? [linux/linux-lts/linux-zen] (default: linux) ",
                vector<string>{"linux", "linux-lts", "linux-zen"}, true, "linux");
        tmp = get_option("Do you want to add linux-firmware? [y-yes/n-no] (default: yes) ", y_n, true, "yes");

        tmp = (tmp == "y" || tmp == "yes") ?
            ("basestrap /mnt " + kernel + " linux-firmware") : ("basestrap /mnt " + kernel);

        system(tmp.c_str());

    FSTAB:
        system("fstabgen -U /mnt >> /mnt/etc/fstab");
        cout << green + "\nFstab generated!\n" + end_color;
        tmp = get_option("Do you want to manually edit it? [y-yes/n-no] (default: no) ", y_n, true, "no");

        if (tmp == "yes" || tmp == "y")
            system("yes | pacman -S vi && vi /mnt/etc/fstab");

    CHROOT:
        cout << blue + "\nChroot into the new artix system!" + end_color << endl;
        system("cp -r startix /mnt");
        system("cp startix-bin /mnt");
        system("artix-chroot /mnt");

    Config:
        cout << blue + "\nSetting the system clock!" + end_color << endl;
        cout << blue + "Select region ('HELP' to see regions) " + end_color;
        get_region(tmp, city);

        symlink(("ln -sf " + tmp).c_str(), "/etc/localtime");
        system("hwclock --systohc");

        cout << blue + "Choose a locale! " + end_color;
        getline(cin, tmp);

        while (true) {
            if (tmp.length() == 0) {
                cout << red + "The locale can't be emtpy ";
                getline(cin, tmp);
            } else if (!accepted_locale(tmp)) {
                cout << red + "This locale is not accepted " + end_color;
                getline(cin, tmp);
            } else
                break;
        }

        system(("echo LANG=" + tmp + " >> /etc/locale.conf").c_str());
        system("echo LC_COLLATE=C >> /etc/locale.conf");

    Bootloader:
        tmp = get_option("\nInstalling bootloader!\nDo you want to install os-prober? [y-yes/n-no] (default: no) ",
                y_n, true, "no");

        (tmp == "yes" || tmp == "y") ?
            system("yes | pacman -S grub os-prober efibootmgr") : system("yes | pacman -S grub efibootmgr");

        tmp = get_option("\nBios or UEFI method? [bios/uefi] ", vector<string>{"bios", "uefi"}, false, "");

        if (tmp == "bios") {
            if (disk.length() == 0)
                get_disk(disk);
            system(("grub-install --recheck /dev/" + disk).c_str());
        } else {
            system("grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub");
        } system("grub-mkconfig -o /boot/grub/grub.cfg");

    User:
        cout << blue + "\nChange tha password for the root user\n" + end_color;
        system("passwd");
        cout << blue + "\nGive a name for the new user (leave it empty to not create one) " + end_color;
        getline(cin, username);

        if (username.length() > 0) {
            system(("useradd -m " + username).c_str());
            system(("passwd " + username).c_str());
            system(("echo " + username + " > /startix/USERNAME").c_str());

            cout << blue + "\nType the groups you want to add the new user in or leave"
                    " it empty to not add the user in any \n(Type 'AUTO' to do this automatically) " + end_color;
            getline(cin, tmp);

            if (tmp == "AUTO") {
                system(("usermod -aG storage,video,audio,kvm " + username).c_str());
                tmp = get_option("\nDo you want to install sudo or doas? [sudo/doas] (default: doas) ",
                        vector<string>{"sudo", "doas"}, true, "doas");

                if (tmp == "sudo") {
                    system("yes | pacman -S sudo");
                    system(("usermod -aG wheel " + username).c_str());
                    system("cp startix/data/sudoers /etc");
                } else {
                    system("yes | pacman -S opendoas");
                    system(("echo \"permit persist keepenv " + username + " as root\" > /etc/doas.conf").c_str());
                }
            } else if (tmp.length() > 0) {
                system(("usermod -aG " + tmp + ' ' + username).c_str());
            }
        }

    Network:
        cout << blue + "\nGive a name for the machine (default: startix) " + end_color;
        getline(cin, hostname);

        if (hostname.length() == 0)
            hostname = "startix";

        system(("echo " + hostname + " > /etc/hostname").c_str());
        system("yes | pacman -S dhcpcd");

        tmp = get_option("\nDo you want to manually create the 'hosts' file? [y-yes/n-no] (default: no) ", y_n, true, "no");

        if (tmp == "no" || tmp == "n") {
            system(("echo \"127.0.0.1      localhost\" > /etc/hosts"));
            system(("echo \"::1            localhost\" >> /etc/hosts"));
            system(("echo \"127.0.1.1      " + hostname + ".localdomain      " + hostname + "\" >> /etc/hosts").c_str());
        } else
            system("yes | pacman -S vi && vi /etc/hosts");

        if (init.length() == 0)
            get_init(init);

        tmp = get_option("\nWhich connman frontend you want to add? [connman-gtk/cmst] (default: connman-gtk) ",
                vector<string>{"connman-gtk", "cmst"}, true, "connman-gtk");

        if (init == "openrc") {
            system(("pacman -S connman-openrc " + tmp).c_str());
            system("rc-update add connmand");
        } else if (init == "runit") {
            system(("pacman -S connman-runit " + tmp).c_str());
            symlink("/etc/runit/sv/connmand", "/etc/runit/runsvdir/default");
        } else {
            system(("pacman -S connman-runit " + tmp).c_str());
            system("s6-rc-bundle -c /etc/s6/rc/compiled add default connmand");
        }

    Extra:
        tmp = get_option("\nDo you want to do anything else with the system? [y-yes/n-no] ", y_n, false, "");

        if (tmp == "yes" || tmp == "y")
            extra(init, username);
        else
            installation_completed();
}
