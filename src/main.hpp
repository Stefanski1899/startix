/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#pragma once

#include "commons.hpp"

bool find_char(const string *s, const char* c);
void installation_completed();
