startix: obj/main.o obj/extra.o obj/get-data.o obj/info.o src/locale/locales.o
	g++ obj/main.o obj/extra.o obj/get-data.o obj/info.o src/locale/locales.o -o startix-bin -std=c++2a

obj/main.o: src/main.cpp
	g++ -c src/main.cpp -o obj/main.o -std=c++2a

obj/extra.o: src/extra.cpp
	g++ -c src/extra.cpp -o obj/extra.o -std=c++2a

obj/get-data.o: src/get-data.cpp
	g++ -c src/get-data.cpp -o obj/get-data.o -std=c++2a

obj/info.o: src/info.cpp
	g++ -c src/info.cpp -o obj/info.o -std=c++2a

src/locale/locales.o: src/locale/locales.cpp
	g++ -c src/locale/locales.cpp -o src/locale/locales.o -std=c++2a

clean:
	rm obj/*.o
	rm startix-bin
