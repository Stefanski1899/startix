/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#include "commons.hpp"

// TODO: Find how I'll see all available keymap options
// void get_keyboard(string *tmp, string *keymap) {
    // cout << blue + "Choose keyboard layout: ('HELP' to see available layouts) " + end_color;
    // while (true) {
        // getline(cin, *tmp);
        // if (*tmp == "HELP") {
            // system("ls -A /usr/share/X11/xkb/symbols/");
        // } else if (fs::is_regular_file("/usr/share/X11/xkb/symbols/" + *tmp)) {
            // *keymap = ("loadkeys " + *tmp);
            // system((*keymap).c_str());
            // break;
        // } else {
            // cout << red +  '\'' + *tmp + "' does not exist, try again " + end_color;
        // }
    // }
// }

string get_option(string prompt, vector<string> options, bool empty, const string &def) {
    string tmp;
    cout << blue + prompt + end_color;
    getline(cin,tmp);

    OPTION_CHECK:
        if (empty && tmp.length() == 0)
            return def;

        for (string o: options) {
            if (tmp == o) {
                return tmp;
            }
        }

    while (true) {
        cout << red + "Not accepted option! Expected ";
        unsigned long count = 1;
        for (string o: options) {
            cout << '\'' + o + "' ";

            if (count < options.size())
                cout << "or ";

            count++;
        } cout << end_color;

        getline(cin,tmp);
        goto OPTION_CHECK;
    }
}

void read_file(string &option, string &filename, char c) {
    char current_char;
    std::ifstream f(filename);

    while (!f.eof()) {
        current_char = f.get();
        if (current_char != c) {
            option += current_char;
        } else
            break;
    }
}

void get_shell(string &shell) {
        string filename = "startix/SHELL";

        if (fs::exists(filename))
            read_file(shell, filename, '\n');
        else {
            shell = get_option("What's the shell you want to use? [bash/zsh] ", vector<string>{"bash/zsh"}, false, "");
            system(("echo " + shell + " > /startix/SHELL").c_str());
        }
}


void get_user(string &username) {
    string filename = "startix/USERNAME";

    if (fs::exists(filename)) {
        read_file(username, filename, ':');
    } else {
        cout << blue + "What's the name of the user you want to operate on? " + end_color;
        getline(cin, username);
        system(("grep " + username + " /etc/passwd > startix/USERNAME").c_str());

        while (fs::is_empty(filename)) {
            cout << "There is no user called '" + username + "', try again ";
            getline(cin, username);
            system(("grep " + username + " /etc/passwd > startix/USERNAME").c_str());
        }
    }
}

void get_init(string &init) {
    string filename = "startix/INIT";

    if (fs::exists(filename)) {
        read_file(init, filename, '\n');
    } else {
        init = get_option("What's your init system? [openrc/runit/s6] ", inits, false, "");
        system(("echo " + init + " > startix/INIT").c_str());
    }
}

void get_disk(string &disk) {
    string filename = "startix/DISK";

    if (fs::exists(filename)) {
        read_file(disk, filename, '\n');
    } else {
        while (true) {
            cout << blue + "Which disk you want to use? (press 'HELP' to see available disks) " + end_color;
            getline(cin, disk);

            if (disk == "HELP")
                system("lsblk");
            else if (!fs::exists("/dev/" + disk))
                cout << red + "There is no disk called: '" + disk + "'\n" + end_color;
            else {
                system(("echo " + disk + " > startix/DISK").c_str());
                break;
            }
        }
    }
}

void get_region(string &tmp, string &city) {
    getline(cin, tmp);

    while (true) {
        if (tmp == "HELP") {
            system("ls -A /usr/share/zoneinfo");
            cout << blue + "Select region ('HELP' to see regions) " + end_color;
            getline(cin, tmp);
        } else if (!fs::exists("/usr/share/zoneinfo/" + tmp)) {
            cout << red + "There is no such option, try again " + end_color;
            getline(cin, tmp);
        } else {
            break;
        }
    }

    if (tmp.find("/") == string::npos) {
        cout << blue + "Select city ('HELP' to see city) " + end_color;
        getline(cin, city);

        while (true) {
            if (city == "HELP") {
                system(("ls -A " + tmp).c_str());
                cout << blue + "Select city ('HELP' to see city) " + end_color;
                getline(cin, city);
            } else if (!fs::exists("/usr/share/zoneinfo/" + tmp + '/' + city)) {
                cout << red + "There is no such option, try again " + end_color;
                getline(cin, city);
            } else {
                tmp = "/usr/share/zoneinfo/" + tmp + '/' + city;
                break;
            }
        }
    }
}
