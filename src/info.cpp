/*
 * This file is part of the Startix project
 *
 * The project is free software licensed under the GNU AGPLv3
 *
 * You can freely redistribute it and/or modify it
 * under the terms of the license agreement
 *
 * */

#include "commons.hpp"

string welcome_massage() {
    string tmp = blue + "Welcome to Startix!\nDo you want to start from the beginning or jump to a specific section?\n";
    tmp += "Type 'START' (or leave it emtpy) to start from the beginning or the section you want to jump ('HELP' to list sections)\n" + end_color;
    return tmp;
}

void help_info() {
    cout << blue + "\nStart/Partitions: create and edit your partitions\n";
    cout << "Format: format partitions\n";
    cout << "Mount: mount partition\n";
    cout << "Internet: add internet connection\n";
    cout << "Base: install the base system\n";
    cout << "Kernel: add a kernel\n";
    cout << "FSTAB: generate fstab\n";
    cout << "CHROOT: chroot into the new system\n";
    cout << "Config: configure the system\n";
    cout << "Bootloader: intsall the bootloader\n";
    cout << "User: add a new user\n";
    cout << "Network: network configuration\n";
    cout << "Extra: do extra stuff or reboot\n";
}
